﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAppTools.Models
{
    public class DataBaseModel
    {
        public string Name { get; set; }
        public Nullable<int> Id { get; set; }
        public Nullable<DateTime> DateCreate { get; set; }
    }
}
