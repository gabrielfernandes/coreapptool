﻿using CoreAppTools.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreAppTools.Rotinas.Helpers;
using CoreAppTools.Rotinas;

namespace CoreAppTools
{
    public partial class MainForm : Form
    {

        #region Module Variables
        string mSSqlDatabase = "";
        #endregion

        #region Constructors
        public MainForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Event Handlers
        private void MainForm_Load(object sender, EventArgs e)
        {
            txtConnectionString.Text = ConfigurationManager.AppSettings["ConnString"];
            chbDb.Checked = true;
            chbSolution.Checked = true;
            chbDapper.Checked = true;
            chbApi.Checked = true;
            chbClient.Checked = true;
            txtNameSpace.Text = @"MyProject";
            txtFolder.Text = @"C:\";
        }
        private void btnGetDBList_Click(object sender, EventArgs e)
        {
            String conxString = txtConnectionString.Text.Trim();
            using (var sqlConx = new SqlConnection(conxString))
            {
                sqlConx.Open();
                var tblDatabases = sqlConx.GetSchema("Databases");
                sqlConx.Close();
                //TRATAR A LISTA DE DATA BASE
                var lista = (from dataRow in tblDatabases.AsEnumerable()
                             orderby dataRow.Field<string>("database_name")
                             where dataRow["database_name"].ToString() != "master"
                             && dataRow["database_name"].ToString() != "model"
                             && dataRow["database_name"].ToString() != "msdb"
                             && dataRow["database_name"].ToString() != "ReportServer$SQLEXPRESS"
                             && dataRow["database_name"].ToString() != "ReportServer$SQLEXPRESSTempDB"
                             && dataRow["database_name"].ToString() != "tempdb"
                             select new DataBaseModel
                             {
                                 Name = dataRow["database_name"].ToString()
                             }).ToList();
                cboDatabases.Items.Add("Selecione o Banco");
                //cboDatabases.SelectedIndex = cboDatabases.Items.Count - 1;
                foreach (var row in lista)
                {
                    cboDatabases.Items.Add(row.Name);
                }


            }
        }
        #endregion



        private void cboDatabases_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboDatabases.Text.Trim() != "Select Database")
                {
                    //if ((cboCustomerName.SelectedValue.ToString().Trim() != "System.Data.DataRowView"))
                    mSSqlDatabase = cboDatabases.Text.Trim();
                    if (txtNameSpace.Text == "MyProject") txtNameSpace.Text = mSSqlDatabase.ToString();

                    string strConn = txtConnectionString.Text.Trim() + ";Initial Catalog=" + mSSqlDatabase;
                    SqlConnection cbConnection = null;
                    try
                    {
                        DataTable dtSchemaTable = new DataTable("Tables");
                        using (cbConnection = new SqlConnection(strConn))
                        {
                            SqlCommand cmdCommand = cbConnection.CreateCommand();
                            cmdCommand.CommandText = "select table_name as Name from INFORMATION_SCHEMA.Tables where TABLE_TYPE ='BASE TABLE'";
                            cbConnection.Open();
                            dtSchemaTable.Load(cmdCommand.ExecuteReader(CommandBehavior.CloseConnection));
                        }
                        treeTables.Nodes.Clear();
                        List<string> Tables = new List<string>();
                        for (int iCount = 0; iCount < dtSchemaTable.Rows.Count; iCount++)
                        {
                            Tables.Add(dtSchemaTable.Rows[iCount][0].ToString());
                        }

                        Tables = (from p in Tables
                                  orderby p
                                  where p != "sysdiagrams"
                                  select p).ToList();

                        treeTables.Nodes.Add("Todos");
                        foreach (var item in Tables)
                        {
                            treeTables.Nodes[0].Nodes.Add(item);
                        }
                    }
                    finally
                    {
                        // ReSharper disable once PossibleNullReferenceException
                        cbConnection.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region TreeView

        // Updates all child tree nodes recursively.
        private void CheckAllChildNodes(TreeNode treeNode, bool nodeChecked)
        {
            foreach (TreeNode node in treeNode.Nodes)
            {
                node.Checked = nodeChecked;
                if (node.Nodes.Count > 0)
                {
                    // If the current node has child nodes, call the CheckAllChildsNodes method recursively.
                    this.CheckAllChildNodes(node, nodeChecked);
                }
            }
        }

        // NOTE   This code can be added to the BeforeCheck event handler instead of the AfterCheck event.
        // After a tree node's Checked property is changed, all its child nodes are updated to the same value.
        private void node_AfterCheck(object sender, TreeViewEventArgs e)
        {
            // The code only executes if the user caused the checked state to change.
            if (e.Action != TreeViewAction.Unknown)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    /* Calls the CheckAllChildNodes method, passing in the current 
                    Checked value of the TreeNode whose checked state changed. */
                    this.CheckAllChildNodes(e.Node, e.Node.Checked);
                }
            }
        }
        #endregion

        #region LocalDestino
        private void btnFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = txtFolder.Text;
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            fbd.Description = $"Selecione a Pasta de Destino";
            fbd.ShowNewFolderButton = true;
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                txtFolder.Text = fbd.SelectedPath;
            }
        }


        #endregion
        #region Gerar
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            escreverLOG($"  Processo Iniciado ... {txtNameSpace.Text}");
            //CRIANDO APLICAÇÂO RAIZ
            escreverLOG($"  Criando o arquivo {txtFolder.Text}");
            var path = $@"{txtFolder.Text}\{txtNameSpace.Text}";
            CriarArquivos(path);

        }

        private void CriarArquivos(string path)
        {
            Core objCoreRotinas = new Core();
            escreverLOG($"  Criando Estrutura Db ...");
            var _typeLayout = Core.LayoutType.Standard;
            if (rdbStandard.Checked) _typeLayout = Core.LayoutType.Standard;
            if (rdbCore.Checked) _typeLayout = Core.LayoutType.Core;
            string selectedPath = path;
            string nameSpace = txtNameSpace.Text;
            string sConString = "";
            sConString = txtConnectionString.Text + " Initial Catalog=" + mSSqlDatabase;
            var objTableNames = GetTablesChecked();
            objCoreRotinas.Read(sConString,
                selectedPath,
                nameSpace,
                objTableNames,
                chbSolution.Checked,
                chbDb.Checked,
                chbDapper.Checked,
                chbApi.Checked,
                chbClient.Checked,
                Core.PluralizeType.Es,
                _typeLayout);
            //GenerateCSharpClasseEntity(PathDb_Models);
        }

        private ArrayList GetTablesChecked()
        {
            var objTables = new ArrayList();
            foreach (TreeNode childNode in treeTables.Nodes[0].Nodes)
            {
                if (childNode.Checked)
                {
                    objTables.Add(childNode.Text.ToString());
                }
            }
            return objTables;
        }

        private void escreverLOG(string v)
        {
            richTextLog.AppendText(v);
            richTextLog.AppendText(System.Environment.NewLine);
        }
        #endregion
    }
}
