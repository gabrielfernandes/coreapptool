﻿using System.Drawing;

namespace CoreAppTools
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtConnectionString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGetDBList = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtFolder = new System.Windows.Forms.TextBox();
            this.btnFolder = new System.Windows.Forms.Button();
            this.chbClient = new System.Windows.Forms.CheckBox();
            this.chbApi = new System.Windows.Forms.CheckBox();
            this.chbDapper = new System.Windows.Forms.CheckBox();
            this.chbSolution = new System.Windows.Forms.CheckBox();
            this.chbDb = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.treeTables = new System.Windows.Forms.TreeView();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cboDatabases = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.richTextLog = new System.Windows.Forms.RichTextBox();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.rdbStandard = new System.Windows.Forms.RadioButton();
            this.rdbCore = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtConnectionString
            // 
            this.txtConnectionString.Location = new System.Drawing.Point(117, 18);
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.Size = new System.Drawing.Size(420, 20);
            this.txtConnectionString.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Connection String";
            // 
            // btnGetDBList
            // 
            this.btnGetDBList.Font = new System.Drawing.Font("Arial", 9F);
            this.btnGetDBList.Location = new System.Drawing.Point(543, 16);
            this.btnGetDBList.Name = "btnGetDBList";
            this.btnGetDBList.Size = new System.Drawing.Size(110, 23);
            this.btnGetDBList.TabIndex = 6;
            this.btnGetDBList.Text = "Carregar Banco";
            this.btnGetDBList.UseVisualStyleBackColor = true;
            this.btnGetDBList.Click += new System.EventHandler(this.btnGetDBList_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.rdbCore);
            this.groupBox2.Controls.Add(this.rdbStandard);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.txtFolder);
            this.groupBox2.Controls.Add(this.btnFolder);
            this.groupBox2.Controls.Add(this.chbClient);
            this.groupBox2.Controls.Add(this.chbApi);
            this.groupBox2.Controls.Add(this.chbDapper);
            this.groupBox2.Controls.Add(this.chbSolution);
            this.groupBox2.Controls.Add(this.chbDb);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtNameSpace);
            this.groupBox2.Location = new System.Drawing.Point(285, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(221, 302);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Configuração";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label5.Location = new System.Drawing.Point(4, 258);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 14);
            this.label5.TabIndex = 27;
            this.label5.Text = "Pasta Destino:";
            // 
            // txtFolder
            // 
            this.txtFolder.Location = new System.Drawing.Point(6, 275);
            this.txtFolder.Name = "txtFolder";
            this.txtFolder.Size = new System.Drawing.Size(155, 20);
            this.txtFolder.TabIndex = 26;
            // 
            // btnFolder
            // 
            this.btnFolder.BackColor = System.Drawing.Color.Transparent;
            this.btnFolder.Location = new System.Drawing.Point(169, 273);
            this.btnFolder.Margin = new System.Windows.Forms.Padding(30);
            this.btnFolder.Name = "btnFolder";
            this.btnFolder.Size = new System.Drawing.Size(43, 23);
            this.btnFolder.TabIndex = 25;
            this.btnFolder.Text = "...";
            this.btnFolder.UseVisualStyleBackColor = false;
            this.btnFolder.Click += new System.EventHandler(this.btnFolder_Click);
            // 
            // chbClient
            // 
            this.chbClient.AutoSize = true;
            this.chbClient.Location = new System.Drawing.Point(6, 228);
            this.chbClient.Name = "chbClient";
            this.chbClient.Size = new System.Drawing.Size(86, 17);
            this.chbClient.TabIndex = 24;
            this.chbClient.Text = "Create Client";
            this.chbClient.UseVisualStyleBackColor = true;
            // 
            // chbApi
            // 
            this.chbApi.AutoSize = true;
            this.chbApi.Location = new System.Drawing.Point(6, 204);
            this.chbApi.Name = "chbApi";
            this.chbApi.Size = new System.Drawing.Size(75, 17);
            this.chbApi.TabIndex = 23;
            this.chbApi.Text = "Create Api";
            this.chbApi.UseVisualStyleBackColor = true;
            // 
            // chbDapper
            // 
            this.chbDapper.AutoSize = true;
            this.chbDapper.Location = new System.Drawing.Point(6, 180);
            this.chbDapper.Name = "chbDapper";
            this.chbDapper.Size = new System.Drawing.Size(95, 17);
            this.chbDapper.TabIndex = 22;
            this.chbDapper.Text = "Create Dapper";
            this.chbDapper.UseVisualStyleBackColor = true;
            // 
            // chbSolution
            // 
            this.chbSolution.AutoSize = true;
            this.chbSolution.Location = new System.Drawing.Point(6, 136);
            this.chbSolution.Name = "chbSolution";
            this.chbSolution.Size = new System.Drawing.Size(98, 17);
            this.chbSolution.TabIndex = 21;
            this.chbSolution.Text = "Create Solution";
            this.chbSolution.UseVisualStyleBackColor = true;
            // 
            // chbDb
            // 
            this.chbDb.AutoSize = true;
            this.chbDb.Location = new System.Drawing.Point(6, 157);
            this.chbDb.Name = "chbDb";
            this.chbDb.Size = new System.Drawing.Size(83, 17);
            this.chbDb.TabIndex = 20;
            this.chbDb.Text = "Create Data";
            this.chbDb.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label4.Location = new System.Drawing.Point(3, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 14);
            this.label4.TabIndex = 19;
            this.label4.Text = "Namespace:";
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Location = new System.Drawing.Point(7, 39);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(205, 20);
            this.txtNameSpace.TabIndex = 6;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.treeTables);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.cboDatabases);
            this.groupBox3.Location = new System.Drawing.Point(12, 75);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(267, 302);
            this.groupBox3.TabIndex = 19;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Banco de Dados";
            // 
            // treeTables
            // 
            this.treeTables.CheckBoxes = true;
            this.treeTables.Location = new System.Drawing.Point(6, 90);
            this.treeTables.Name = "treeTables";
            this.treeTables.Size = new System.Drawing.Size(253, 206);
            this.treeTables.TabIndex = 7;
            this.treeTables.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.node_AfterCheck);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label2.Location = new System.Drawing.Point(-3, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 14);
            this.label2.TabIndex = 8;
            this.label2.Text = "Selecione as Tabelas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 14);
            this.label3.TabIndex = 16;
            this.label3.Text = "Selecione as Tabelas:";
            // 
            // cboDatabases
            // 
            this.cboDatabases.FormattingEnabled = true;
            this.cboDatabases.Location = new System.Drawing.Point(6, 39);
            this.cboDatabases.Name = "cboDatabases";
            this.cboDatabases.Size = new System.Drawing.Size(253, 21);
            this.cboDatabases.TabIndex = 15;
            this.cboDatabases.SelectedIndexChanged += new System.EventHandler(this.cboDatabases_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnGetDBList);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtConnectionString);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(660, 57);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Conexção";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.richTextLog);
            this.groupBox4.Controls.Add(this.btnGenerate);
            this.groupBox4.Location = new System.Drawing.Point(512, 75);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(199, 302);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Gerar";
            // 
            // richTextLog
            // 
            this.richTextLog.Location = new System.Drawing.Point(6, 19);
            this.richTextLog.Name = "richTextLog";
            this.richTextLog.Size = new System.Drawing.Size(187, 231);
            this.richTextLog.TabIndex = 1;
            this.richTextLog.Text = "";
            // 
            // btnGenerate
            // 
            this.btnGenerate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(188)))), ((int)(((byte)(155)))));
            this.btnGenerate.Location = new System.Drawing.Point(6, 256);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(187, 40);
            this.btnGenerate.TabIndex = 0;
            this.btnGenerate.Text = "INICIAR";
            this.btnGenerate.UseVisualStyleBackColor = false;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // rdbStandard
            // 
            this.rdbStandard.AutoSize = true;
            this.rdbStandard.Checked = true;
            this.rdbStandard.Location = new System.Drawing.Point(6, 90);
            this.rdbStandard.Name = "rdbStandard";
            this.rdbStandard.Size = new System.Drawing.Size(40, 17);
            this.rdbStandard.TabIndex = 28;
            this.rdbStandard.TabStop = true;
            this.rdbStandard.Text = "v.1";
            this.rdbStandard.UseVisualStyleBackColor = true;
            // 
            // rdbCore
            // 
            this.rdbCore.AutoSize = true;
            this.rdbCore.Location = new System.Drawing.Point(52, 90);
            this.rdbCore.Name = "rdbCore";
            this.rdbCore.Size = new System.Drawing.Size(40, 17);
            this.rdbCore.TabIndex = 29;
            this.rdbCore.Text = "v.2";
            this.rdbCore.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 8.25F);
            this.label6.Location = new System.Drawing.Point(6, 73);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 14);
            this.label6.TabIndex = 17;
            this.label6.Text = "Framework:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 387);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Core AppTools";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtConnectionString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGetDBList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtFolder;
        private System.Windows.Forms.Button btnFolder;
        private System.Windows.Forms.CheckBox chbClient;
        private System.Windows.Forms.CheckBox chbApi;
        private System.Windows.Forms.CheckBox chbDapper;
        private System.Windows.Forms.CheckBox chbSolution;
        private System.Windows.Forms.CheckBox chbDb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNameSpace;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TreeView treeTables;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboDatabases;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.RichTextBox richTextLog;
        private System.Windows.Forms.RadioButton rdbCore;
        private System.Windows.Forms.RadioButton rdbStandard;
        private System.Windows.Forms.Label label6;
    }
}

