﻿using CoreAppTools.Rotinas.Helpers;
using DatabaseSchemaReader.DataSchema;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CoreAppTools.Rotinas.Helpers.SQLSchema;

namespace CoreAppTools.Rotinas
{
    public class Core
    {
        public Core() { }
        private PluralizeType _typePluralize;
        private LayoutType _typeLayout;
        private FileUtils _fileFunctions = new FileUtils();

        public void Read(
            string connectionString,
            string pathDestination,
            string nameSpace = "Base",
            ArrayList tableNames = null,
            bool createProj = true,
            bool createData = true,
            bool createDapper = true,
            bool createApi = true,
            bool createClient = true,
            PluralizeType typePluralize = PluralizeType.List,
            LayoutType typeLayout = LayoutType.Standard)
        {
            SQLSchema objSql = new SQLSchema();
            _typePluralize = typePluralize;
            _typeLayout = typeLayout;

            var tables = objSql.ReadTables(connectionString, tableNames);

            if (_typeLayout == LayoutType.Junix)
            {

            }
            else if (_typeLayout == LayoutType.Standard)
            {
                #region Constructor DotNet
                //There are no datatables, and the structure is identical for all providers.
                foreach (var table in tables)
                {
                    if (createData)
                    {
                        string strEntityClass = CreateEntity(table, nameSpace);
                        string strClassMapping = CreateMapping(table, nameSpace);
                        string strClassService = CreateService(table, nameSpace, _typeLayout);

                        CreateFile(strEntityClass, GetClassName(table.Name), pathDestination, nameSpace, DirectoryType.Entities);
                        CreateFile(strClassMapping, GetClassName(table.Name, DirectoryType.Mappin), pathDestination, nameSpace, DirectoryType.Mappin);
                        CreateFile(strClassService, GetClassName(table.Name, DirectoryType.Service), pathDestination, nameSpace, DirectoryType.Service);
                    }
                    if (createApi)
                    {
                        string strClassController = CreateController(table, nameSpace, _typeLayout);
                        CreateFile(strClassController, GetClassName(table.Name, DirectoryType.ApiController), pathDestination, nameSpace, DirectoryType.ApiController);

                        string strClassViewModel = CreateViewModel(table, nameSpace);
                        CreateFile(strClassViewModel, GetClassName(table.Name, DirectoryType.ApiController), pathDestination, nameSpace, DirectoryType.ApiController);
                    }
                }
                if (createProj)
                {
                    //string strDataSolution = CreateDataSln(tables, nameSpace, _typeLayout);
                    //CreateFile(strDataSolution, GetSolutionName(nameSpace, DirectoryType.DataSolution), pathDestination, nameSpace, DirectoryType.DataSolution);

                    //string strServiceSolution = CreateServiceSln(tables, nameSpace, _typeLayout);
                    //CreateFile(strServiceSolution, GetSolutionName(nameSpace, DirectoryType.ServiceSolution), pathDestination, nameSpace, DirectoryType.ServiceSolution);

                    //string strRepositorySolution = CreateRepositorySln(nameSpace);
                    //CreateFile(strRepositorySolution, GetSolutionName(nameSpace, DirectoryType.Repository), pathDestination, nameSpace, DirectoryType.Repository);
                }
                if (createData)
                {
                    #region BaseEntity
                    string strBaseEntityClass = string.Empty;
                    strBaseEntityClass = CreateBaseEntity(nameSpace);
                    CreateFile(strBaseEntityClass, GetClassName("BaseEntity", DirectoryType.Core), pathDestination, nameSpace, DirectoryType.Core);
                    #endregion

                    #region Repository

                    string strAplicationContextClass = string.Empty;
                    strAplicationContextClass = CreateRepositoryContext(tables, nameSpace);
                    CreateFile(strAplicationContextClass, GetClassName("ApplicationContext", DirectoryType.Context), pathDestination, nameSpace, DirectoryType.Context);

                    string strIRepositoryClass = string.Empty;
                    strIRepositoryClass = CreateIRepository(nameSpace);
                    CreateFile(strIRepositoryClass, GetClassName("IRepository", DirectoryType.IRepository), pathDestination, nameSpace, DirectoryType.IRepository);

                    string strRepositoryClass = string.Empty;
                    strRepositoryClass = CreateRepository(nameSpace);
                    CreateFile(strRepositoryClass, GetClassName("Repository", DirectoryType.Repository), pathDestination, nameSpace, DirectoryType.Repository);
                    #endregion
                }
                #endregion
            }
            else if (_typeLayout == LayoutType.Core)
            {
                #region Core
                //There are no datatables, and the structure is identical for all providers.
                foreach (var table in tables)
                {

                    if (createData)
                    {
                        string strEntityClass = string.Empty;
                        string strClassMapping = string.Empty;
                        string fv = string.Empty;

                        strEntityClass = CreateEntity(table, nameSpace);
                        strClassMapping = CreateMapping(table, nameSpace);

                        CreateFile(strEntityClass, GetClassName(table.Name), pathDestination, nameSpace, DirectoryType.Entities);
                        CreateFile(strClassMapping, GetClassName(table.Name, DirectoryType.Mappin), pathDestination, nameSpace, DirectoryType.Mappin);
                    }
                    if (createProj)
                    {
                        string strClassService = CreateService(table, nameSpace, _typeLayout);
                        CreateFile(strClassService, GetClassName(table.Name, DirectoryType.Service), pathDestination, nameSpace, DirectoryType.Service);
                    }
                    if (createApi)
                    {
                        string strClassController = CreateController(table, nameSpace, _typeLayout);
                        CreateFile(strClassController, GetClassName(table.Name, DirectoryType.ApiController), pathDestination, nameSpace, DirectoryType.ApiController);

                        string strClassViewModel = CreateViewModel(table, nameSpace);
                        CreateFile(strClassViewModel, GetClassName(table.Name, DirectoryType.ApiController), pathDestination, nameSpace, DirectoryType.ApiController);
                    }

                }
                if (createProj)
                {
                    string strDataSolution = CreateDataSln(tables, nameSpace, _typeLayout);
                    CreateFile(strDataSolution, GetSolutionName(nameSpace, DirectoryType.DataSolution), pathDestination, nameSpace, DirectoryType.DataSolution);

                    string strServiceSolution = CreateServiceSln(tables, nameSpace, _typeLayout);
                    CreateFile(strServiceSolution, GetSolutionName(nameSpace, DirectoryType.ServiceSolution), pathDestination, nameSpace, DirectoryType.ServiceSolution);

                    string strRepositorySolution = CreateRepositorySln(nameSpace);
                    CreateFile(strRepositorySolution, GetSolutionName(nameSpace, DirectoryType.Repository), pathDestination, nameSpace, DirectoryType.Repository);
                }
                if (createData)
                {
                    #region BaseEntity
                    string strBaseEntityClass = string.Empty;
                    strBaseEntityClass = CreateBaseEntity(nameSpace);
                    CreateFile(strBaseEntityClass, GetClassName("BaseEntity", DirectoryType.Core), pathDestination, nameSpace, DirectoryType.Core);

                    string strAplicationContextClass = string.Empty;
                    strAplicationContextClass = CreateRepositoryContext(tables, nameSpace);
                    CreateFile(strAplicationContextClass, GetClassName("ApplicationContext", DirectoryType.Context), pathDestination, nameSpace, DirectoryType.Context);
                    #endregion

                    #region Repository
                    string strIRepositoryClass = string.Empty;
                    strIRepositoryClass = CreateIRepository(nameSpace);
                    CreateFile(strIRepositoryClass, GetClassName("IRepository", DirectoryType.IRepository), pathDestination, nameSpace, DirectoryType.IRepository);

                    string strRepositoryClass = string.Empty;
                    strRepositoryClass = CreateRepository(nameSpace);
                    CreateFile(strRepositoryClass, GetClassName("Repository", DirectoryType.Repository), pathDestination, nameSpace, DirectoryType.Repository);
                    #endregion
                }
                #endregion

            }




        }


        private string GetClassName(string name)
        {
            return GetClassName(name, DirectoryType.Entities);
        }
        private string GetClassName(string name, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.Mappin:
                    return $"{name}Map.cs";
                case DirectoryType.Repository:
                    return $"{name}.cs";
                case DirectoryType.Service:
                    return $"{name}Service.cs";
                case DirectoryType.MVCView:
                    return $"{name}.cshtml";
                case DirectoryType.ApiController:
                    return $"{name}Controller.cs";
                default:
                    return $"{name}.cs";
            }

        }
        private string GetSolutionName(string nameSpace, DirectoryType type)
        {
            switch (type)
            {
                case DirectoryType.DataSolution:
                    return $"{nameSpace}.Data.csproj";
                case DirectoryType.ServiceSolution:
                    return $"{nameSpace}.Service.csproj";
                case DirectoryType.Repository:
                    return $"{nameSpace}.Repository.csproj";
                case DirectoryType.API:
                    return $"{nameSpace}.Api.csproj";
                default:
                    return $"{nameSpace}.csproj";
            }

        }

        #region CREATE FILE
        private void CreateFile(string strClass, string className, string pathDestination, string nameSpace, DirectoryType type)
        {
            CreateFile(strClass, className, pathDestination, nameSpace, type, string.Empty);
        }
        private void CreateFile(string strClass, string className, string pathDestination, string nameSpace, DirectoryType type, string nmFolderView)
        {
            string direcotory = DirectoryTypeDesc(type);

            pathDestination = Path.Combine(pathDestination, $"{nameSpace}.{direcotory}");

            if (type == DirectoryType.MVCView)
            {
                pathDestination = Path.Combine(pathDestination, nmFolderView);
            }

            _fileFunctions.CreateFileText(pathDestination, className, strClass);
        }
        #endregion

        #region WRITE TEMPLATE
        private string CreateBaseEntity(string nameSpace)
        {
            Templates.data.BaseEntityTemplate page = new Templates.data.BaseEntityTemplate();
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }

        private string CreateEntity(DatabaseTable table, string nameSpace)
        {
            Templates.data.EntityTemplate page = new Templates.data.EntityTemplate(table);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateMapping(DatabaseTable table, string nameSpace)
        {
            Templates.data.MappingTemplate page = new Templates.data.MappingTemplate(table);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }

        private string CreateService(DatabaseTable table, string nameSpace, LayoutType _layoutType)
        {
            Templates.service.ServiceTemplate page = new Templates.service.ServiceTemplate(table, _layoutType);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;

        }
        private string CreateDataSln(List<DatabaseTable> tables, string nameSpace, LayoutType _layoutType)
        {
            Templates.solutions.DataSolutionTemplate page = new Templates.solutions.DataSolutionTemplate(tables, _layoutType);
            var param = new Dictionary<string, object>() { { "NameSpace", "" } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateServiceSln(List<DatabaseTable> tables, string nameSpace, LayoutType _layoutType)
        {
            Templates.solutions.DataSolutionTemplate page = new Templates.solutions.DataSolutionTemplate(tables, _layoutType);
            var paramNameSpace = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            var param = new Dictionary<string, object>() { { "strDataSolution", $@"..\{nameSpace}.Data\{nameSpace}.Data.csproj" } };
            var param1 = new Dictionary<string, object>() { { "strRepositorySolution", $@"..\{nameSpace}.Repository\{nameSpace}.Repository.csproj" } };
            page.Session = paramNameSpace;
            page.Session = param;
            page.Session = param1;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;

        }
        private string CreateRepositorySln(string nameSpace)
        {
            Templates.solutions.RepositorySolutionTemplate page = new Templates.solutions.RepositorySolutionTemplate();
            var param = new Dictionary<string, object>() { { "strDataSolution", $@"..\{nameSpace}.Data\{nameSpace}.Data.csproj" } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;

        }
        private string CreateRepositoryContext(List<DatabaseTable> tables, string nameSpace)
        {
            Templates.repository.ApplicationContextTemplate page = new Templates.repository.ApplicationContextTemplate(tables);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateIRepository(string nameSpace)
        {
            Templates.repository.IRepositoryTemplate page = new Templates.repository.IRepositoryTemplate();
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateRepository(string nameSpace)
        {
            Templates.repository.RepositoryTemplate page = new Templates.repository.RepositoryTemplate();
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateController(DatabaseTable table, string nameSpace, LayoutType _layoutType)
        {
            Templates.api.ControllerTemplate page = new Templates.api.ControllerTemplate(table, _layoutType);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        private string CreateViewModel(DatabaseTable table, string nameSpace)
        {
            Templates.api.ViewModelTemplate page = new Templates.api.ViewModelTemplate(table);
            var param = new Dictionary<string, object>() { { "NameSpace", nameSpace } };
            page.Session = param;
            page.Initialize();
            String pageContent = page.TransformText();
            return pageContent;
        }
        #endregion

        #region ENUMERADORES
        public enum LayoutType
        {
            Junix = 1,
            Standard = 2,
            Core = 3
        }
        public enum PluralizeType
        {
            Es = 1,
            List = 2
        }
        private string NameSpace(DirectoryType en)
        {
            switch (en)
            {
                case DirectoryType.API:
                    return "API";
                //return "Models.Entities";
                case DirectoryType.Context:
                    return "Infra.Context";
                case DirectoryType.Core:
                    return "Core.Entites";
                case DirectoryType.Mappin:
                    return "Infra.Mapping";
                case DirectoryType.Entities:
                    return "Domain.Entities";
                case DirectoryType.IRepository:
                    return "Domain.Repositories";
                case DirectoryType.Repository:
                    return "Infra.Repositories";
                case DirectoryType.Service:
                    return "Domain.Services";
                default:
                    return string.Empty;
            }
        }
        private string DirectoryTypeDesc(DirectoryType en)
        {
            switch (en)
            {
                case DirectoryType.API:
                    return "API";
                case DirectoryType.ApiController:
                    return "Api/Controllers";
                case DirectoryType.Context:
                    return "Infra/Context";
                case DirectoryType.Core:
                    return "Core/Entities";
                case DirectoryType.Mappin:
                    return "Infra/Mappings";
                case DirectoryType.Repository:
                    return "Infra/Repositories";
                case DirectoryType.IRepository:
                    return "Domain/Repositories";
                case DirectoryType.Service:
                    return "Domain/Services";
                case DirectoryType.Entities:
                    return "Domain/Entities";
                default:
                    return "";
            }
        }
        private enum DirectoryType
        {

            API = 1,
            ApiController = 2,
            Context = 3,
            Core = 4,
            Mappin = 5,
            Repository = 6,
            IRepository = 7,
            Service = 9,
            Entities = 10,
            DataSolution = 11,
            MVCView = 12,
            ServiceSolution = 13
        }
        #endregion
    }
}
