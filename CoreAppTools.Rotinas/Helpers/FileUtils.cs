﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAppTools.Rotinas.Helpers
{
    public class FileUtils
    {
        public bool CreateFileText(string path, string fileName, string content)
        {
            CreateDirectory(path);

            string fullName = Path.Combine(path, fileName);

            FileStream fs = null;
            if (File.Exists(fullName))
            {
                File.Delete(fullName);
            }

            if (!File.Exists(fullName))
            {
                fs = new FileStream(fullName, FileMode.OpenOrCreate, FileAccess.Write);
            }
            else
                fs = new FileStream(fullName, FileMode.Append);

            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);

            sw.Write(content);

            sw.Close();
            fs.Close();

            return true;
        }

        private void CreateDirectory(string path)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public void CreateFileIfNotExists(string fullName, string content)
        {

            FileStream fs = null;

            if (!File.Exists(fullName))
            {
                fs = new FileStream(fullName, FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs);

                sw.Write(content);

                sw.Close();
                fs.Close();
            }
        }

        public string ReadyFileText(string fullName)
        {
            string content = string.Empty;
            using (FileStream fs = new FileStream(fullName, FileMode.OpenOrCreate, FileAccess.Read))
            {
                StreamReader sr = new StreamReader(fs);
                content = sr.ReadToEnd();
            }
            return content;
        }

        public void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            CreateDirectory(sourceDirName);

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        internal void DeleteAll(string pathDestination)
        {
            System.IO.DirectoryInfo di = new DirectoryInfo(pathDestination);

            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }
    }
}
