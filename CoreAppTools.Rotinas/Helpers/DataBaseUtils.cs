﻿using DatabaseSchemaReader.DataSchema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreAppTools.Rotinas.Helpers
{
    public class DataBaseUtils
    {

        public object GetColumnType(DatabaseColumn column)
        {
            string type = string.Empty;

            switch (column.DbDataType)
            {
                case "bigint": { type = "long"; break; }
                case "binary": { type = "byte[]"; break; }
                case "bit": { type = "bool"; break; }
                case "char": { type = "string"; break; }
                case "date": { type = "DateTime"; break; }
                case "datetime": { type = "System.DateTime"; break; }
                case "datetime2": { type = "System.DateTime"; break; }
                case "datetimeoffset": { type = "DateTimeOffset"; break; }
                case "decimal": { type = "decimal"; break; }
                case "float": { type = "float"; break; }
                case "image": { type = "byte[]"; break; }
                case "int": { type = "int"; break; }
                case "money": { type = "decimal"; break; }
                case "nchar": { type = "string"; break; }
                case "ntext": { type = "string"; break; }
                case "numeric": { type = "decimal"; break; }
                case "nvarchar": { type = "string"; break; }
                case "real": { type = "double"; break; }
                case "smalldatetime": { type = "DateTime"; break; }
                case "smallint": { type = "short"; break; }
                case "smallmoney": { type = "decimal"; break; }
                case "text": { type = "string"; break; }
                case "time": { type = "TimeSpan"; break; }
                case "timestamp": { type = "DateTime"; break; }
                case "tinyint": { type = "byte"; break; }
                case "uniqueidentifier": { type = "Guid"; break; }
                case "varbinary": { type = "byte[]"; break; }
                case "varchar": { type = "string"; break; }
                default:
                    break;
            }

            if (column.Nullable && type != "string")
            {
                if (type == "byte[]")
                {
                    type = $"{type}";
                }
                else
                {
                    type = $"Nullable<{type}>";
                }

            }
            return type;
        }

        public string PluralizeName(string name)
        {
            var setName = Inflector.Inflector.Pluralize(name) ?? name;
            return setName;
        }
        public string SingularizeName(string name)
        {
            var setName = Inflector.Inflector.Singularize(name) ?? name;
            return setName;
        }

        public string GetColumnName(string name)
        {
            if (name.Contains("FK_"))
            {
                return name.Split('_')[name.Split('_').Length - 1];
            }

            return name.Replace(" ", "_");
        }
        public string GetColumnNamePluralize(string name)
        {
            name = name.Replace(" ", "_");

            char n = name[name.Length - 1];
            if (n == 'o')
                name += "s";
            else if (name == "s")
                name = name += "";
            else
                name += "s";

            return name;
        }

        public enum DirectoryType
        {

            API = 1,
            ApiController = 2,
            Context = 3,
            Core = 4,
            Mappin = 5,
            Repository = 6,
            IRepository = 7,
            Service = 9,
            Entities = 10,
            DataSolution = 11,
            MVCView = 12,
            ServiceSolution = 13
        }
        public string NameSpace(DirectoryType en)
        {
            switch (en)
            {
                case DirectoryType.API:
                    return "API";
                //return "Models.Entities";
                case DirectoryType.Context:
                    return "Infra.Context";
                case DirectoryType.Core:
                    return "Core.Entites";
                case DirectoryType.Mappin:
                    return "Infra.Mapping";
                case DirectoryType.Entities:
                    return "Domain.Entities";
                case DirectoryType.IRepository:
                    return "Domain.Repositories";
                case DirectoryType.Repository:
                    return "Infra.Repositories";
                case DirectoryType.Service:
                    return "Domain.Services";
                default:
                    return string.Empty;
            }
        }
        public string DirectoryTypeDesc(DirectoryType en)
        {
            switch (en)
            {
                case DirectoryType.API:
                    return "API";
                case DirectoryType.ApiController:
                    return "Api/Controllers";
                case DirectoryType.Context:
                    return "Infra/Context";
                case DirectoryType.Core:
                    return "Core/Entities";
                case DirectoryType.Mappin:
                    return "Infra/Mappings";
                case DirectoryType.Repository:
                    return "Domain/Repositories";
                case DirectoryType.IRepository:
                    return "Domain/Repositories";
                case DirectoryType.Service:
                    return "Domain/Services";
                case DirectoryType.Entities:
                    return "Domain/Entities";
                default:
                    return string.Empty;
            }
        }







    }
}
