﻿using DatabaseSchemaReader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseSchemaReader.DataSchema;
using System.IO;
using System.Reflection;
using System.IO.Compression;
using System.Text.RegularExpressions;
using System.Collections;

namespace CoreAppTools.Rotinas.Helpers
{
    public class SQLSchema
    {
        private string[] _ignoreTables = { "sysdiagrams", "tb_" };
        public SQLSchema() { }

        private FileUtils _fileFunctions = new FileUtils();

        public List<DatabaseTable> ReadTables(string connectionString, ArrayList tableNames = null)
        {
            //To use it simply specify the connection string and ADO provider (eg System.Data,SqlClient or System.Data.OracleClient)
            string providername = "System.Data.SqlClient";
            //Create the database reader object.
            var dbReader = new DatabaseReader(connectionString, providername);
            //Then load the schema (this will take a little time on moderate to large database structures)
            var schema = dbReader.ReadAll();
            var tables = schema.Tables.Where(x => tableNames.Contains(x.Name) && !_ignoreTables.Contains(x.Name));
            return tables.ToList();
        }
       
    }
}
