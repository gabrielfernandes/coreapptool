﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseSchemaReader.DataSchema;
using static CoreAppTools.Rotinas.Core;

namespace CoreAppTools.Rotinas.Templates.solutions
{
    partial class DataSolutionTemplate
    {
        private List<DatabaseTable> tables_data;
        private LayoutType _typeLayout;
        public DataSolutionTemplate(List<DatabaseTable> tables, LayoutType typeLayout) {
            this.tables_data = tables;
            this._typeLayout = typeLayout;
        }
    }
}
