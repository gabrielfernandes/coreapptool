﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatabaseSchemaReader.DataSchema;
using static CoreAppTools.Rotinas.Core;

namespace CoreAppTools.Rotinas.Templates.service
{
    partial class ServiceTemplate
    {
        private DatabaseTable t_data;
        private LayoutType _typeLayout;

        public ServiceTemplate(DatabaseTable data, LayoutType typeLayout) {
            this.t_data = data;
            this._typeLayout = typeLayout;
        }
    }
}
