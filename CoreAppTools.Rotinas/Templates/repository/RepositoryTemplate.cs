﻿// ------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version: 15.0.0.0
//  
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
// ------------------------------------------------------------------------------
namespace CoreAppTools.Rotinas.Templates.repository
{
    using System.Linq;
    using System.Text;
    using System.Collections.Generic;
    using CoreAppTools.Rotinas.Helpers;
    using DatabaseSchemaReader;
    using DatabaseSchemaReader.DataSchema;
    using System;
    
    /// <summary>
    /// Class to produce the template output
    /// </summary>
    
    #line 1 "C:\Projetos\CoreAppTools\CoreAppTools.Rotinas\Templates\repository\RepositoryTemplate.tt"
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TextTemplating", "15.0.0.0")]
    public partial class RepositoryTemplate : RepositoryTemplateBase
    {
#line hidden
        /// <summary>
        /// Create the template output
        /// </summary>
        public virtual string TransformText()
        {
            
            #line 10 "C:\Projetos\CoreAppTools\CoreAppTools.Rotinas\Templates\repository\RepositoryTemplate.tt"
  DataBaseUtils utils = new DataBaseUtils(); 
            
            #line default
            #line hidden
            this.Write("using System;\r\nusing System.ComponentModel.DataAnnotations;\r\nusing System.Linq;\r\n" +
                    "using System.Linq.Expressions;\r\nusing Microsoft.EntityFrameworkCore;\r\nusing Micr" +
                    "osoft.EntityFrameworkCore.ChangeTracking;\r\nusing CoreSolution.Data;\r\n\r\nnamespace" +
                    " ");
            
            #line 19 "C:\Projetos\CoreAppTools\CoreAppTools.Rotinas\Templates\repository\RepositoryTemplate.tt"
            this.Write(this.ToStringHelper.ToStringWithCulture(NameSpace));
            
            #line default
            #line hidden
            this.Write(".");
            
            #line 19 "C:\Projetos\CoreAppTools\CoreAppTools.Rotinas\Templates\repository\RepositoryTemplate.tt"
            this.Write(this.ToStringHelper.ToStringWithCulture(utils.NameSpace(DataBaseUtils.DirectoryType.Repository)));
            
            #line default
            #line hidden
            this.Write("\r\n{\r\n    public class Repository<TEntity> : IRepository<TEntity>, IDisposable\r\n  " +
                    "  where TEntity : BaseEntity\r\n    {\r\n        private readonly ApplicationContext" +
                    " Context;\r\n\r\n        public Repository(ApplicationContext dbContext)\r\n        {\r" +
                    "\n            Context = dbContext;\r\n        }\r\n\r\n        public virtual TEntity I" +
                    "nsert(TEntity entity)\r\n        {\r\n            if (entity == null)\r\n            {" +
                    "\r\n                throw new ArgumentNullException(\"entity\");\r\n            }\r\n   " +
                    "         Context.Set<TEntity>().Add(entity);\r\n            Context.SaveChanges();" +
                    "\r\n            return entity;\r\n        }\r\n\r\n        public virtual TEntity Update" +
                    "(TEntity entity)\r\n        {\r\n            if (entity == null)\r\n            {\r\n   " +
                    "             throw new ArgumentNullException(\"entity\");\r\n            }\r\n        " +
                    "    Context.Entry(entity).State = EntityState.Modified;\r\n            return enti" +
                    "ty;\r\n        }\r\n\r\n        public virtual void Delete(long id)\r\n        {\r\n      " +
                    "      if (id == 0)\r\n            {\r\n                throw new ArgumentNullExcepti" +
                    "on(\"entity\");\r\n            }\r\n            var item = Context.Set<TEntity>().Find" +
                    "(id);\r\n\r\n            item.DateExcluded = DateTime.Now;\r\n            this.InsertO" +
                    "rUpdate(item);\r\n\r\n            this.SaveChanges();\r\n\r\n        }\r\n\r\n        public" +
                    " virtual void Delete(TEntity entity)\r\n        {\r\n            if (entity == null)" +
                    "\r\n            {\r\n                throw new ArgumentNullException(\"entity\");\r\n   " +
                    "         }\r\n            Context.Set<TEntity>().Remove(entity);\r\n        }\r\n\r\n   " +
                    "     public virtual void Delete(Expression<Func<TEntity, bool>> where)\r\n        " +
                    "{\r\n            var objects = Context.Set<TEntity>().Where(where).AsEnumerable();" +
                    "\r\n            foreach (var item in objects)\r\n            {\r\n                Cont" +
                    "ext.Set<TEntity>().Remove(item);\r\n            }\r\n        }\r\n\r\n        public vir" +
                    "tual TEntity FindById(long id)\r\n        {\r\n            return Context.Set<TEntit" +
                    "y>().Find(id);\r\n        }\r\n\r\n        public virtual TEntity FindOne(Expression<F" +
                    "unc<TEntity, bool>> where = null)\r\n        {\r\n            var all = FindAll(wher" +
                    "e).FirstOrDefault();\r\n            return FindAll(where).FirstOrDefault();\r\n     " +
                    "   }\r\n\r\n        public IQueryable<T> Set<T>() where T : class\r\n        {\r\n      " +
                    "      return Context.Set<T>();\r\n        }\r\n\r\n        public virtual IQueryable<T" +
                    "Entity> FindAll(Expression<Func<TEntity, bool>> where = null)\r\n        {\r\n      " +
                    "      return null != where ? Context.Set<TEntity>().Where(x => !x.DateExcluded.H" +
                    "asValue).Where(where) : Context.Set<TEntity>().Where(x => !x.DateExcluded.HasVal" +
                    "ue);\r\n        }\r\n\r\n        public virtual IQueryable<TEntity> All()\r\n        {\r\n" +
                    "            return Context.Set<TEntity>().Where(x => !x.DateExcluded.HasValue);\r" +
                    "\n        }\r\n\r\n        public IQueryable<TEntity> GetAll()\r\n        {\r\n          " +
                    "  return Context.Set<TEntity>().AsNoTracking();\r\n        }\r\n\r\n        public vir" +
                    "tual bool SaveChanges()\r\n        {\r\n            return 0 < Context.SaveChanges()" +
                    ";\r\n        }\r\n\r\n        /// <summary>\r\n        /// Releases all resources used b" +
                    "y the Entities\r\n        /// </summary>\r\n        public void Dispose()\r\n        {" +
                    "\r\n            if (null != Context)\r\n            {\r\n                Context.Dispo" +
                    "se();\r\n            }\r\n        }\r\n\r\n        public virtual TEntity InsertOrUpdate" +
                    "(TEntity entity)\r\n        {\r\n\r\n            if (entity.Id == 0)\r\n            {\r\n " +
                    "               entity.DateRegister = DateTime.Now;\r\n                entity = thi" +
                    "s.Insert(entity);\r\n            }\r\n\r\n            else\r\n            {\r\n           " +
                    "     var baseEntity = Context.Set<TEntity>().AsNoTracking().FirstOrDefault(x => " +
                    "x.Id == entity.Id);\r\n                entity.DateModified = DateTime.Now;\r\n\r\n    " +
                    "            Context.Entry(entity).State = EntityState.Modified;\r\n               " +
                    " EntityEntry<TEntity> entityEntry = Context.Entry(entity);\r\n\r\n                if" +
                    " (entityEntry.State == EntityState.Detached)\r\n                {\r\n               " +
                    "     Context.Set<TEntity>().Attach(entity);\r\n\r\n                    entity.DateRe" +
                    "gister = baseEntity.DateRegister;\r\n                    entityEntry.State = Entit" +
                    "yState.Modified;\r\n                }\r\n            }\r\n\r\n            this.SaveChang" +
                    "es();\r\n            return entity;\r\n        }\r\n\r\n        public virtual Validatio" +
                    "nContext ValidateEntity(TEntity entity)\r\n        {\r\n            var validationCo" +
                    "ntext = new ValidationContext(entity);\r\n            Validator.ValidateObject(ent" +
                    "ity, validationContext);\r\n            return validationContext;\r\n        }\r\n    " +
                    "}\r\n}\r\n");
            return this.GenerationEnvironment.ToString();
        }
        
        #line 1 "C:\Projetos\CoreAppTools\CoreAppTools.Rotinas\Templates\repository\RepositoryTemplate.tt"

private string _NameSpaceField;

/// <summary>
/// Access the NameSpace parameter of the template.
/// </summary>
private string NameSpace
{
    get
    {
        return this._NameSpaceField;
    }
}


/// <summary>
/// Initialize the template
/// </summary>
public virtual void Initialize()
{
    if ((this.Errors.HasErrors == false))
    {
bool NameSpaceValueAcquired = false;
if (this.Session.ContainsKey("NameSpace"))
{
    this._NameSpaceField = ((string)(this.Session["NameSpace"]));
    NameSpaceValueAcquired = true;
}
if ((NameSpaceValueAcquired == false))
{
    object data = global::System.Runtime.Remoting.Messaging.CallContext.LogicalGetData("NameSpace");
    if ((data != null))
    {
        this._NameSpaceField = ((string)(data));
    }
}


    }
}


        
        #line default
        #line hidden
    }
    
    #line default
    #line hidden
    #region Base class
    /// <summary>
    /// Base class for this transformation
    /// </summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.TextTemplating", "15.0.0.0")]
    public class RepositoryTemplateBase
    {
        #region Fields
        private global::System.Text.StringBuilder generationEnvironmentField;
        private global::System.CodeDom.Compiler.CompilerErrorCollection errorsField;
        private global::System.Collections.Generic.List<int> indentLengthsField;
        private string currentIndentField = "";
        private bool endsWithNewline;
        private global::System.Collections.Generic.IDictionary<string, object> sessionField;
        #endregion
        #region Properties
        /// <summary>
        /// The string builder that generation-time code is using to assemble generated output
        /// </summary>
        protected System.Text.StringBuilder GenerationEnvironment
        {
            get
            {
                if ((this.generationEnvironmentField == null))
                {
                    this.generationEnvironmentField = new global::System.Text.StringBuilder();
                }
                return this.generationEnvironmentField;
            }
            set
            {
                this.generationEnvironmentField = value;
            }
        }
        /// <summary>
        /// The error collection for the generation process
        /// </summary>
        public System.CodeDom.Compiler.CompilerErrorCollection Errors
        {
            get
            {
                if ((this.errorsField == null))
                {
                    this.errorsField = new global::System.CodeDom.Compiler.CompilerErrorCollection();
                }
                return this.errorsField;
            }
        }
        /// <summary>
        /// A list of the lengths of each indent that was added with PushIndent
        /// </summary>
        private System.Collections.Generic.List<int> indentLengths
        {
            get
            {
                if ((this.indentLengthsField == null))
                {
                    this.indentLengthsField = new global::System.Collections.Generic.List<int>();
                }
                return this.indentLengthsField;
            }
        }
        /// <summary>
        /// Gets the current indent we use when adding lines to the output
        /// </summary>
        public string CurrentIndent
        {
            get
            {
                return this.currentIndentField;
            }
        }
        /// <summary>
        /// Current transformation session
        /// </summary>
        public virtual global::System.Collections.Generic.IDictionary<string, object> Session
        {
            get
            {
                return this.sessionField;
            }
            set
            {
                this.sessionField = value;
            }
        }
        #endregion
        #region Transform-time helpers
        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void Write(string textToAppend)
        {
            if (string.IsNullOrEmpty(textToAppend))
            {
                return;
            }
            // If we're starting off, or if the previous text ended with a newline,
            // we have to append the current indent first.
            if (((this.GenerationEnvironment.Length == 0) 
                        || this.endsWithNewline))
            {
                this.GenerationEnvironment.Append(this.currentIndentField);
                this.endsWithNewline = false;
            }
            // Check if the current text ends with a newline
            if (textToAppend.EndsWith(global::System.Environment.NewLine, global::System.StringComparison.CurrentCulture))
            {
                this.endsWithNewline = true;
            }
            // This is an optimization. If the current indent is "", then we don't have to do any
            // of the more complex stuff further down.
            if ((this.currentIndentField.Length == 0))
            {
                this.GenerationEnvironment.Append(textToAppend);
                return;
            }
            // Everywhere there is a newline in the text, add an indent after it
            textToAppend = textToAppend.Replace(global::System.Environment.NewLine, (global::System.Environment.NewLine + this.currentIndentField));
            // If the text ends with a newline, then we should strip off the indent added at the very end
            // because the appropriate indent will be added when the next time Write() is called
            if (this.endsWithNewline)
            {
                this.GenerationEnvironment.Append(textToAppend, 0, (textToAppend.Length - this.currentIndentField.Length));
            }
            else
            {
                this.GenerationEnvironment.Append(textToAppend);
            }
        }
        /// <summary>
        /// Write text directly into the generated output
        /// </summary>
        public void WriteLine(string textToAppend)
        {
            this.Write(textToAppend);
            this.GenerationEnvironment.AppendLine();
            this.endsWithNewline = true;
        }
        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void Write(string format, params object[] args)
        {
            this.Write(string.Format(global::System.Globalization.CultureInfo.CurrentCulture, format, args));
        }
        /// <summary>
        /// Write formatted text directly into the generated output
        /// </summary>
        public void WriteLine(string format, params object[] args)
        {
            this.WriteLine(string.Format(global::System.Globalization.CultureInfo.CurrentCulture, format, args));
        }
        /// <summary>
        /// Raise an error
        /// </summary>
        public void Error(string message)
        {
            System.CodeDom.Compiler.CompilerError error = new global::System.CodeDom.Compiler.CompilerError();
            error.ErrorText = message;
            this.Errors.Add(error);
        }
        /// <summary>
        /// Raise a warning
        /// </summary>
        public void Warning(string message)
        {
            System.CodeDom.Compiler.CompilerError error = new global::System.CodeDom.Compiler.CompilerError();
            error.ErrorText = message;
            error.IsWarning = true;
            this.Errors.Add(error);
        }
        /// <summary>
        /// Increase the indent
        /// </summary>
        public void PushIndent(string indent)
        {
            if ((indent == null))
            {
                throw new global::System.ArgumentNullException("indent");
            }
            this.currentIndentField = (this.currentIndentField + indent);
            this.indentLengths.Add(indent.Length);
        }
        /// <summary>
        /// Remove the last indent that was added with PushIndent
        /// </summary>
        public string PopIndent()
        {
            string returnValue = "";
            if ((this.indentLengths.Count > 0))
            {
                int indentLength = this.indentLengths[(this.indentLengths.Count - 1)];
                this.indentLengths.RemoveAt((this.indentLengths.Count - 1));
                if ((indentLength > 0))
                {
                    returnValue = this.currentIndentField.Substring((this.currentIndentField.Length - indentLength));
                    this.currentIndentField = this.currentIndentField.Remove((this.currentIndentField.Length - indentLength));
                }
            }
            return returnValue;
        }
        /// <summary>
        /// Remove any indentation
        /// </summary>
        public void ClearIndent()
        {
            this.indentLengths.Clear();
            this.currentIndentField = "";
        }
        #endregion
        #region ToString Helpers
        /// <summary>
        /// Utility class to produce culture-oriented representation of an object as a string.
        /// </summary>
        public class ToStringInstanceHelper
        {
            private System.IFormatProvider formatProviderField  = global::System.Globalization.CultureInfo.InvariantCulture;
            /// <summary>
            /// Gets or sets format provider to be used by ToStringWithCulture method.
            /// </summary>
            public System.IFormatProvider FormatProvider
            {
                get
                {
                    return this.formatProviderField ;
                }
                set
                {
                    if ((value != null))
                    {
                        this.formatProviderField  = value;
                    }
                }
            }
            /// <summary>
            /// This is called from the compile/run appdomain to convert objects within an expression block to a string
            /// </summary>
            public string ToStringWithCulture(object objectToConvert)
            {
                if ((objectToConvert == null))
                {
                    throw new global::System.ArgumentNullException("objectToConvert");
                }
                System.Type t = objectToConvert.GetType();
                System.Reflection.MethodInfo method = t.GetMethod("ToString", new System.Type[] {
                            typeof(System.IFormatProvider)});
                if ((method == null))
                {
                    return objectToConvert.ToString();
                }
                else
                {
                    return ((string)(method.Invoke(objectToConvert, new object[] {
                                this.formatProviderField })));
                }
            }
        }
        private ToStringInstanceHelper toStringHelperField = new ToStringInstanceHelper();
        /// <summary>
        /// Helper to produce culture-oriented representation of an object as a string
        /// </summary>
        public ToStringInstanceHelper ToStringHelper
        {
            get
            {
                return this.toStringHelperField;
            }
        }
        #endregion
    }
    #endregion
}
